package labs.vex.intellij.plugin.phantom.actions;

import com.intellij.icons.AllIcons;
import com.intellij.ide.actions.CreateFileFromTemplateAction;
import com.intellij.ide.actions.CreateFileFromTemplateDialog;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiDirectory;

public class CreateNewFileAction extends CreateFileFromTemplateAction {

    public CreateNewFileAction() {
        super("Phantom SDK File", "Create new Phantom SDK file", AllIcons.Icons.Ide.SpeedSearchPrompt);
    }

    @Override
    protected void buildDialog(Project project, PsiDirectory directory, CreateFileFromTemplateDialog.Builder builder) {
        builder
            .setTitle("New Phantom SDK File")
            .addKind("TeleOp game unit", AllIcons.Icons.Ide.NextStep, "GameUnitTeleOp.kt")
            .addKind("Autonomous game unit", AllIcons.Icons.Ide.NextStep, "GameUnitAuto.kt")
            .addKind("Game module", AllIcons.Icons.Ide.NextStep, "GameModule.kt")
            .addKind("Game assembly", AllIcons.Icons.Ide.NextStep, "GameAssembly.kt");
    }

    @Override
    protected String getActionName(PsiDirectory directory, String newName, String templateName) {
        return "Create new Phantom SDK File: " + newName;
    }
}
